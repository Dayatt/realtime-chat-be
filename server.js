const express = require('express')
const http = require('http')
const socketIo = require('socket.io')

const port = process.env.PORT || 4001
const index = require('./routes/index')

const app = express()
app.use(index)

const server = http.createServer(app)
const io = socketIo(server)

io.on('connection', socket => {
    console.log("New client connected")

    socket.on('typing', typing => {
        io.sockets.emit('typing', typing)
    })

    socket.on('update chat', chat => {
        io.sockets.emit('update chat', chat)
    })

    socket.on('new user', user => {
        io.sockets.emit('new user', user)
    })
    
    socket.on('disconnect', () => console.log("Client disconnected"))
})

server.listen(port, () => console.log(`Listening to port ${port}`))
